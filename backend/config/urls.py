from django.contrib import admin
from django.urls import path, re_path, include
from apps.post.views import IndexView

urlpatterns = [
    re_path(r'^(?!(post|admin|account)).*$', IndexView.as_view()),
    path('admin/', admin.site.urls),
    path('account/', include('account.urls')),
    path('post/', include('post.urls')),
]
