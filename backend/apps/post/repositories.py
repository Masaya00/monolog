from post.models import Post
from account.models import User


class PostRepository:
    def __init__(self, model_cls: Post = Post) -> None:
        self.model_cls = model_cls

    def get_by_id(self, id) -> Post | None:
        post = self.model_cls.objects.filter(id=id).first()
        return post

    def get_post_list(self, search_word: str = None) -> list[Post]:
        posts = self.model_cls.objects.all().order_by("-updated_at")
        if search_word:
            posts = posts.filter(content__icontains=search_word)
        return list(posts)

    def create(self, user: User, content: str) -> Post:
        post = self.model_cls(
            user=user,
            content=content
        )
        post.save()
        return post

    def delete_by_id(self, id: int) -> None:
        deleted = self.model_cls.objects.filter(id=id).delete()
        return deleted

    def update_by_id(self, id:int, content: str) -> Post:
        print(2)
        print(content)
        post = self.get_by_id(id)
        if post:
            post.content = content
            post.save()
        return post
