from rest_framework import serializers
from post.models import Post
from account.models import User

class PostSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    content = serializers.CharField()
    created_at = serializers.DateTimeField(format='%Y年%m月%d日 %H時%M分', read_only=True)
    updated_at = serializers.DateTimeField(format='%Y-%m-%d %H:%M', read_only=True)
    username = serializers.CharField(source='user.username', read_only=True)


class PostAddSerializer(serializers.Serializer):
    username = serializers.CharField()
    content = serializers.CharField()

class PostEditSerializer(serializers.Serializer):
    content = serializers.CharField()

