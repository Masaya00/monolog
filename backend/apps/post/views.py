import json
from typing import Any
from django.shortcuts import render
from django.views import View
from django.db import transaction
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from post.repositories import PostRepository
from post.post import Post
from post.serializers import PostSerializer, PostAddSerializer, PostEditSerializer
from account.repositories import UserRepository
from utils.exceptions import BadParameter

# Create your views here.
class IndexView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'index.html', {})

class PostListAPI(APIView):
    def get(self, request):
        post = PostRepository()
        search_word = request.GET.get('search')
        posts = post.get_post_list(search_word=search_word)
        serializered_post = PostSerializer(posts, many=True)
        return Response(serializered_post.data)


class PostAddAPI(APIView):
    """投稿登録用API
    """

    def post(self, request):
        serializer = PostAddSerializer(data=request.data)
        if not serializer.is_valid():
            raise BadParameter(
                message='データの読み込みに失敗しました'
            )
        valid_data = serializer.validated_data
        post = Post()
        post.add(username=valid_data['username'], content=valid_data['content'])
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class PostDetailAPI(APIView):
    """投稿詳細用API
    """

    def get(self, request, id):
        post_app = Post()
        post = post_app.get_by_id(id=id)
        serializered_post = PostSerializer(post)
        return Response(serializered_post.data)

    def delete(self, request, id):
        try:
            post_app = Post()
            deleted = post_app.delete_by_id(id)
        except Exception as e:
            print(e)
            raise Exception
        return Response(deleted, status=status.HTTP_200_OK)

    def put(self, request, id):
        serializer = PostEditSerializer(data=request.data)
        if not serializer.is_valid():
            raise BadParameter
        data = serializer.validated_data
        print(data['content'])
        try:
            post_app = Post()
            post = post_app.updated_by_id(id=id, content=data['content'])
        except Exception as e:
            print(e)
            raise Exception
        print(3)
        print(post)
        return Response({"result": True}, status=status.HTTP_200_OK)



