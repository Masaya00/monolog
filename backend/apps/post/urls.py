from django.urls import re_path, path
from post.views import PostListAPI, PostAddAPI, PostDetailAPI

urlpatterns = [
    # 投稿
    path('api/list/', PostListAPI.as_view(), name='post_list'),
    path('api/add/', PostAddAPI.as_view(), name='post_add'),
    path('api/<int:id>/', PostDetailAPI.as_view(), name='post_detail'),
]

