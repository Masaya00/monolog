from django.db import transaction
from post.repositories import PostRepository
from account.repositories import UserRepository
from post.models import Post


class Post:
    def __init__(self) -> None:
        self.repo = PostRepository()
        self.user_repo = UserRepository()

    def add(self, username: str, content: str):
        """
        投稿の追加を行う
        """
        try:
            with transaction.atomic():
                user = self.user_repo.get_by_name(username)
                if user:
                    self.repo.create(user, content)
                else:
                    raise Exception
        except Exception as e:
            print(e)

    def get_by_id(self, id: int) -> Post | None:
        post = self.repo.get_by_id(id)
        return post

    def delete_by_id(self, id: int) -> None:
        deleted = self.repo.delete_by_id(id)
        return deleted

    def updated_by_id(self, id: int, content: str) -> None:
        print(content)
        with transaction.atomic():
            post = self.repo.update_by_id(id, content)
            return post
