from django.db import models
from django.db.models.query import QuerySet
from account.models import User

class BaseModelQuerySet(models.QuerySet):
    def be(self) -> QuerySet:
        return self.filter(is_deleted=False)


class BaseModel(models.Model):
    created_at = models.DateTimeField('作成日時', auto_now_add=True)
    updated_at = models.DateTimeField('更新日時', auto_now=True)
    is_deleted = models.BooleanField('削除フラグ', default=False)

    objects = BaseModelQuerySet.as_manager()

    class Meta:
        abstract = True


class Post(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField('内容')

    class Meta:
        db_table = 'post'
        verbose_name = '投稿'
        verbose_name_plural ='投稿'
    
    def __str__(self):
        return self.user.username

class RePost(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=models.CASCADE)

    class Meta:
        unique_together = ('user', 'post')

    class Meta:
        db_table = 'repost'


class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=models.CASCADE)

    class Meta:
        unique_together = ('user', 'post')
        db_table = 'like'

