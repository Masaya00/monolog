from account.models import User
from account.utils import hashing_password

class UserRepository:
    def __init__(self, model_cls: User = User) -> None:
        self.model_cls = model_cls

    def get_by_name(self, name: str) -> User | None:
        """usernameからUserオブジェクトを取得
        :param name: ユーザー名
        :return 対象のユーザーオブジェクト
        """
        user = User.objects.filter(username=name).first()
        return user

    def get_by_uuid(self, uuid: str) -> User | None:
        """
        :param id: ユーザーid
        :return 対象のユーザー
        """
        user = User.objects.filter(id=uuid).first()
        return user

    def create(self, username: str, password: str) -> User:
        """
        :param username: ユーザーID
        :param password: パスワード
        :return 作成したユーザー
        """
        user = self.model_cls(
            username=username,
            password=hashing_password(password)
        )
        user.save()
        return user

    def update_username(self, old_username: str, new_username: str) -> User:
        """
        :param old_username: 旧ユーザー名
        :param new_username: 新ユーザー名
        :return 更新後ユーザー
        """
        user = self.get_by_name(old_username)
        user.username = new_username
        user.save()
        return user
