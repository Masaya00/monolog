import uuid
from django.db import models
from django.db.models.query import QuerySet
from account.const import PrefectureCode


class BaseModelQuerySet(models.QuerySet):
    def be(self) -> QuerySet:
        return self.filter(is_deleted=False)

class BaseModel(models.Model):
    created_at = models.DateTimeField('作成日時', auto_now_add=True)
    updated_at = models.DateTimeField('更新日時', auto_now=True)
    is_deleted = models.BooleanField('削除フラグ', default=False)

    objects = BaseModelQuerySet.as_manager()

    class Meta:
        abstract = True


class User(BaseModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    username = models.CharField('ユーザー名', max_length=50, unique=True)
    password = models.CharField('パスワード', max_length=256)

    class Meta:
        db_table = 'user'
        verbose_name = 'ユーザー'
        verbose_name_plural = 'ユーザー'

    def __str__(self) -> str:
        return self.username


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.TextField(null=True)
    birthday = models.DateField('生年月日', null=True)
    prefecture = models.IntegerField('都道府県', choices=PrefectureCode.choices, null=True)
    website = models.CharField('WEBサイト', max_length=100, null=True)

    class Meta:
        db_table = 'user_profile'


class Follow(models.Model):
    follower = models.ForeignKey(User, on_delete=models.CASCADE, related_name='following') # フォローされている人
    followed = models.ForeignKey(User, on_delete=models.CASCADE, related_name='followers') # フォローしている人
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'follow'
