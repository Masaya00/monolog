from django.shortcuts import render
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import exceptions
from account.models import User
from account.serializers import UserSerializer
from account.account import Users
from account.utils import hashing_password
from account.const import USER_CREATED, USER_UPDATED, FAIL_USER_CREATED

class UserList(APIView):
    """ユーザー一覧 TODO:デバッグ用"""
    def get(self, request):
        user = User
        users = User.objects.be()
        serialized_user = UserSerializer(users, many=True)
        return Response(serialized_user.data)


class UserDetail(APIView):
    """ユーザー詳細"""
    def get(self, request, username: str):
        user = Users()
        user.get_by_name(username)
        return Response({'user': user})

    def put(self, request, username: str):
        new_username = request.POST.get('username')
        user = Users()
        user.update_username(username, new_username)
        return Response(USER_UPDATED, status=status.HTTP_200_OK)


class UserCreate(APIView):
    """新規ユーザー登録"""
    def post(self, request):
        user = Users()
        username = request.POST.get('username')
        password = request.POST.get('password')
        try:
            user.add(username, password)
            return Response(status=status.HTTP_201_CREATED)
        except Exception as e:
            print(e)
            # TODO: エラーメッセージどうしようか
            return Response(FAIL_USER_CREATED, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
