from django.db import transaction, IntegrityError
from account.repositories import UserRepository
from account.models import User


class Users:
    def __init__(self) -> None:
        self.repo = UserRepository()

    def add(self, username: str, password: str):
        """
        ユーザー登録を行う
        :params username: ユーザー名
        :params password: パスワード
        :return None
        """
        try:
            with transaction.atomic():
                self.repo.create(username, password)
        except Exception as e:
            print('IntegrityError')

    def get_by_name(self, username: str) -> User:
        return self.repo.get_by_name(username)

    @transaction.atomic
    def update_username(self, old_username: str, new_username: str) -> User:
        user = self.repo.update_username(old_username, new_username)
        return user