from django.urls import path
from account.views import UserList, UserCreate, UserDetail
from rest_framework_simplejwt.views import TokenObtainPairView


urlpatterns = [
    path('api/list/', UserList.as_view(), name='users'),
    path('api/create/', UserCreate.as_view(), name='create'),
    path('api/<str:username>', UserDetail.as_view(), name='detail'),
    # JWT生成
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
]
