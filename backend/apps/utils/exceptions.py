class ApplicationError(Exception):
    """Base class for exceptions in this module."""

    message = "Application error"
    code = "E000"
    data = None

    def __init__(self, message=None, code=None, data=None):
        if message:
            self.message = message
        if code:
            self.code = code
        if data:
            self.data = data



class BadParameter(ApplicationError):
    message = "Validation Error for post data"
    code = "E001"
    data = None
