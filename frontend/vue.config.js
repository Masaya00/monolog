const BundleTracker = require("webpack-bundle-tracker");

module.exports = {
  publicPath: "http://0.0.0.0:8080/",
  outputDir: "./dist/",

  configureWebpack: (config) => {
    config.plugins.push(new BundleTracker({ filename: "webpack-stats.json" }));
    config.output.filename = "bundle.js";
    config.optimization.splitChunks = false;
  },

  chainWebpack: (config) => {
    config.resolve.alias
      .set("__STATIC__", "static");
  },

  devServer: {
    hot: true,
    https: false,
    allowedHosts: 'all',
    headers: {
      "Access-Control-Allow-Origin": ["*"]
    }
  }
};
