import { createStore } from 'vuex';
import createPersistedState from 'vuex-persistedstate';
const store = createStore({
  state: {
    user: null,
    isAuthenticated: false, //ログイン状態
    flashMessage: '', //フラッシュメッセージ
  },
  // storeの状態を変更するための関数
  mutations: {
    setFlashMessage(state, message) {
      state.flashMessage = message
    },
    setIsAuthenticated(state, isAuthenticated) {
      state.isAuthenticated = isAuthenticated
    },
    setUser(state, user) {
      state.user = user;
    },
  },
  actions: {
    setFlashMessage({ commit }, message) {
      commit('setFlashMessage', message);
      setTimeout(() => {
        commit('setFlashMessage', '');
      }, 1000); // 3秒後にフラッシュメッセージをリセット
    },
    setIsAuthenticated({commit}, isAuthenticated) {
      commit('setIsAuthenticated', isAuthenticated)
    },
    setUser({commit}, setUser) {
      commit('setUser', setUser)
    },
  },
  // Vuex PersistedStateでvuexの情報を永続化
  plugins: [createPersistedState()]
});

export default store;
