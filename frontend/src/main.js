import { createApp } from 'vue'
import App from './App.vue'
import router from '@/router';
import './index.css'
import store from '@/store/store'

const app = createApp(App)

// routerの追加を追記
app.use(router);
// vuexの使用
app.use(store)
app.mount('#app')
