import { CSRF_TOKEN } from "@/utils/csrf_token.js";
const handleResponse = (response) => {
  if (response.status == 204) {
    return ''
  } else if (response.status == 404) {
    return null
  } else {
    return response.json()
  }
}

// api用汎用関数
const apiService = (endpoint, method, data, queryParams) => {
  // クエリ文字列を構築
  const queryString = new URLSearchParams(queryParams).toString();
  // クエリ文字列をエンドポイントに追加
  const endpointWithQuery = queryString ? `${endpoint}?${queryString}` : endpoint;
  const config = {
    method: method || 'GET',
    headers: {
      "content-type": "application/json",
      "X-CSRFTOKEN": CSRF_TOKEN,
    },
  }
  // GETリクエストの場合、リクエストボディは設定しない
  if (method !== 'GET') {
    config.body = JSON.stringify(data);
  }
  return fetch(endpointWithQuery, config).then(handleResponse);
}

// ログイン用api
const loginApi = (endpoint, method, data) => {
  const config = {
    method: method || 'GET',
    body: data !== undefined ? JSON.stringify(data) : null,
    headers: {
      "content-type": "application/json",
      "X-CSRFTOKEN": CSRF_TOKEN,
    },
  }

  return fetch(endpoint, config)
}

export { apiService, loginApi };