import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import App from '@/App.vue';
import Login from '@/views/Login'
import Home from '@/views/Home'
import store from '@/store/store'


const routes = [
  {
    path: '/',
    name: 'login-component',
    component: Login,
  },
  {
    path: '/home',
    name: 'home-component',
    component: Home,
    meta: { requiresAuth: true },
  }
]

const router = createRouter(
  {
    history: createWebHistory(),
    routes,
  }
)

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    // ログイン状態をチェックし、ログインしていない場合はリダイレクトなどの処理を行う
    if (!store.state.isAuthenticated) {
      // ログインしていない場合の処理
      // store.dispatch('setFlashMessage', 'ログインしてください');
      next({ path: '/' });
    } else {
      // ログインしている場合はそのまま次のルートに進む
      next();
    }
  } else {
    // requiresAuthが設定されていない場合はそのまま次のルートに進む
    next();
  }
});

createApp(App)
  .use(router)
  .mount('#app')

export default router;
